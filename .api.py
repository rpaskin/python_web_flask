#!flask/bin/python
from flask import Flask, jsonify, abort, make_response, request
import csv
import pandas as pd

app = Flask(__name__)

# =============== DATA ==================
# Task list:
tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol',
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    }
]

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

# =============== ROUTES =================
# ---------------- TEXT ------------------
@app.route('/')
def index():
    return "Olá Globo Lab PIT'ers!"

# ----------------- API -------------------
@app.route('/todo/api/v1.0/tasks', methods=['GET'])
def get_tasks():
    return jsonify({'tasks': tasks})

@app.route('/todo/api/v1.0/tasks/<int:task_id>', methods=['GET'])
def get_task(task_id):
    task = [task for task in tasks if task['id'] == task_id]
    if len(task) == 0:
        abort(404)
    return jsonify({'task': task[0]})

@app.route('/todo/api/v1.0/tasks', methods=['POST'])
def create_task():
    if not request.json or not 'title' in request.json:
        abort(400)
    task = {
        'id': tasks[-1]['id'] + 1,
        'title': request.json['title'],
        'description': request.json.get('description', ""),
        'done': False
    }
    tasks.append(task)
    return jsonify({'task': task}), 201

@app.route('/movies', methods=['GET'])
def get_movies():
    csv_file = pd.DataFrame(pd.read_csv("movies.csv", sep = ",", header = 0, index_col = False))
    json_file = csv_file.to_json(orient = "records", date_format = "epoch", double_precision = 10, force_ascii = True, date_unit = "ms", default_handler = None)
    response = app.response_class(
        response=json_file,
        status=200,
        mimetype='application/json'
    )
    return response


# csvfile = 'movies.csv'
# fieldnames = ("Release Year","Title","Origin/Ethnicity","Director","Cast","Genre","Wiki Page","Plot")
# reader = csv.DictReader( csvfile, fieldnames)
# for row in reader:
#     json.dump(row, jsonfile)

#     jsonfile.write('\n')

# with open(file) as fh:
#     rd = csv.DictReader(fh, delimiter=',')
#     for row in rd:
#         print(row)

if __name__ == '__main__':
    app.run(debug=True)