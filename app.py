#!flask/bin/python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return "Oi pessoal!"

@app.route('/pit')
def pit():
    return "pit pit pit!"

if __name__ == '__main__':
    app.run(debug=True)
